#ifndef xAODAnaHelpers_FatJet_H
#define xAODAnaHelpers_FatJet_H

#include "xAODAnaHelpers/Particle.h"
#include "xAODAnaHelpers/Jet.h"

namespace xAH {

  class FatJet : public Particle
    {
    public:

      // scale
      float JetConstitScaleMomentum_eta;
      float JetConstitScaleMomentum_phi;
      float JetConstitScaleMomentum_m;
      float JetConstitScaleMomentum_pt;

      float JetEMScaleMomentum_eta;
      float JetEMScaleMomentum_phi;
      float JetEMScaleMomentum_m;
      float JetEMScaleMomentum_pt;

      // area
      float GhostArea;
      float ActiveArea;
      float VoronoiArea;
      float ActiveArea4vec_pt;
      float ActiveArea4vec_eta;
      float ActiveArea4vec_phi;
      float ActiveArea4vec_m;

      // substructure
      float  Split12;
      float  Split23;
      float  Split34;
      float  Tau1_wta;
      float  Tau2_wta;
      float  Tau3_wta;
      float  Tau21_wta;
      float  Tau32_wta;
      float  ECF1;
      float  ECF2;
      float  ECF3;
      float  C2;
      float  D2;
      float  NTrimSubjets;
      int    NClusters;
      int    nTracks;

      // additional substructure variables
      float  Angularity;
      float  Aplanarity;
      float  Dip12;
      float  Dip13;
      float  Dip23;
      float  DipExcl12;
      float  FoxWolfram0;
      float  FoxWolfram1;
      float  FoxWolfram2;
      float  FoxWolfram3;
      float  FoxWolfram4;
      float  KtDR;
      float  Mu12;
      float  PlanarFlow;
      float  Sphericity;
      float  Tau1;
      float  Tau2;
      float  Tau3;
      float  Tau21;
      float  Tau32;
      float  ThrustMaj;
      float  ThrustMin;
      float  ZCut12;
      float  ZCut23;
      float  ZCut34;

      // constituent
      int    numConstituents;

      // constituentAll
      std::vector<float>  constituentWeights;
      std::vector<float>  constituent_pt;
      std::vector<float>  constituent_eta;
      std::vector<float>  constituent_phi;
      std::vector<float>  constituent_e;

      // bosons 
      int nTQuarks;
      int nHBosons;
      int nWBosons;
      int nZBosons;

      // VTag
      int Wtag_medium;
      int Ztag_medium;

      int Wtag_tight;
      int Ztag_tight;

      std::vector<xAH::Jet> trkJets;
      
    };

}//xAH
#endif // xAODAnaHelpers_Particle_H
